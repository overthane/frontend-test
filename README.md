# Frontend Test for Global Payments #

Built with Primitive UI framework

## Essay

#### Describe a time where you have stepped up from your role to either learn new things or take a project to a successful state. What did you learn? What was the outcome you expected and the final outcome?

Working at Originate, roughly two weeks before the launch of [The Campus Launch Event](http://new.100archive.com/project/the-campus-launch-event-and-marketing-suite) the client inquired about the possibility of displaying a large video across multiple different TV screens. Upon research, myself and the Head of Digital narrowed the solution to two alternatives:

1. A ready-to-use device which you can rent for several thousand euro **per month**; or
2. Build our own solution with the use of Raspberry Pi devices and [PiWall](https://piwall.co.uk/), an open source solution.

I accepted the challenge of option #2 since we still had some time until the event. After purchasing everything (4x Raspberry Pi units, HDMI cables, power supplies) I went about building it.

Documentation was hit-or-miss, and after a week I had scoured the internet for solutions, from dodgy and mispelled guides to discontinued dependency packages.

During this project I learned, **on-the-fly**, and implemented:

* Raspberry Pi hardware and software, as well as how to make it run with as little power consumption as possible
* basic Python programming
* more than I ever thought I'd have to learn about CRON jobs

At the beginning I expected a couple of days to set it up, which ended up being a little over a week because of incomplete/incorrect information in the official documentation. To my advantage, my Bash knowledge was sharp!

In the end I [documented my solution](https://docs.google.com/document/d/1-qdGayDxchoq2yweIP3ZcPnJ444WeWmUfSyNXtUACvI/edit?usp=sharing), and installed it at the event's location. All of which will continue running until that last building is sold or one of the units' components break.